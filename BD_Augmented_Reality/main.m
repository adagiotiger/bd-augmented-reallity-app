//
//  main.m
//  BD_Augmented_Reality
//
//  Created by Linda Chan on 12/03/2015.
//  Copyright (c) 2015 Linda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
