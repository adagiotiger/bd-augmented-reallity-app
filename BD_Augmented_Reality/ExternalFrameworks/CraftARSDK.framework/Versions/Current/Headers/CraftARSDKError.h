// This file is free software. You may use it under the MIT license, which is copied
// below and available at http://opensource.org/licenses/MIT
//
// Copyright (c) 2014 Catchoom Technologies S.L.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

#import <Foundation/Foundation.h>

NSString *craftarSDKErrorDomain;
typedef enum CraftARSDKErrorCode : NSInteger CraftARSDKErrorCode;
/**
 Error code identifiyng the problem
 */
enum CraftARSDKErrorCode : NSInteger {
    CRAFTAR_SEARCH_UNKNOWN_ERROR,
    CRAFTAR_CLOUD_SERVER_ERROR,
    // Token errors
    CRAFTAR_TOKEN_MISSING,
    CRAFTAR_WRONG_TOKEN_FORMAT,
    CRAFTAR_INVALID_TOKEN,
    // Image errors
    CRAFTAR_IMAGE_MISSING,
    CRAFTAR_IMAGE_TOO_LARGE,
    CRAFTAR_IMAGE_DIMENSIONS_TOO_SMALL,
    CRAFTAR_IMAGE_HAS_TRANSPARENCY,
    CRAFTAR_IMAGE_NO_DETAILS,
    CRAFTAR_IMAGE_NOT_LOADED,
    // AR errors
    CRAFTAR_AR_REFERENCE_ERROR_UNKNOWN,
    CRAFTAR_AR_REFERENCE_ERROR_INTERNAL,
    CRAFTAR_AR_REFERENCE_ERROR_INVALID_DATA,
    CRAFTAR_AR_REFERENCE_ERROR_DATA_GENERATING,
    CRAFTAR_AR_REFERENCE_ERROR_LIMIT_EXCEEDED,
    CRAFTAR_AR_REFERENCE_ERROR_WRONG_LICENSE,
    // Content erorrs
    CRAFTAR_CONTENT_ERROR_DOWNLOAD_MODEL,
};

/**
 A CraftARSDK Error is an NSError with error codes of the type CraftARSDKErrorCode and a message
 describing the problem.
 @see CraftARSDKError.h for error codes.
 */
@interface CraftARSDKError : NSError

///@cond
+ (CraftARSDKError *) errorWithCodeString: (NSString *) errorCodeString andMessage: (NSString *) message;
+ (CraftARSDKError *) errorWithCode: (CraftARSDKErrorCode) errorCode andMessage: (NSString *) message;
///@endcond
@end
