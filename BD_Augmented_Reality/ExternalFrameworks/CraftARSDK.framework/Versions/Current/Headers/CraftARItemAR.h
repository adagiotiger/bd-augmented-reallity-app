// This file is free software. You may use it under the MIT license, which is copied
// below and available at http://opensource.org/licenses/MIT
//
// Copyright (c) 2014 Catchoom Technologies S.L.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

#import <CraftARSDK/CraftARItem.h>
#import <CraftARSDK/CraftARTrackingContentVideo.h>
#import <CraftARSDK/CraftARTrackingContentImage.h>
#import <CraftARSDK/CraftARTrackingContentImageButton.h>
#import <CraftARSDK/CraftARTrackingContent3dModel.h>

extern NSString* CraftARTrackingARItemObservationContext;

/**
 * A CraftARARItem is intended to display an augmented reality scene
 * on top of the reference image.
 */
@interface CraftARItemAR : CraftARItem {
    ///@cond
    NSString *referenceData;
    int referenceId;
    NSObject *result;
    Boolean isTracked;
}

@property (nonatomic, readonly) NSString *referenceData;
@property (nonatomic, readwrite) int referenceId;
@property (nonatomic, readwrite) NSObject *result;
@property (nonatomic, readwrite) Boolean isTracked;
///@endcond

/**
 * Specifies the translation applied to the item in the rendering scene
 */
@property (nonatomic, readwrite) CATransform3D itemTranslation;

/**
 * Specifies the rotation applied to the item in the rendering scene
 */
@property (nonatomic, readwrite) CATransform3D itemRotation;

/**
 * Specifies whether the item contents have to be drawn in the scene even if the item is not being tracked.
 * False by default
 */
@property (nonatomic, readwrite) Boolean drawOffTracking;


/**
 Initialize an empty AR item with tracking data to be able to create a local AR experience
 @param resource name for the resource file containing the tracking data
 @param type extension for the resource file
 */
- (id) initFromResource:(NSString *)resource ofType:(NSString *)type;

/**
 Add content to the AR item
 @param content an instance of CraftARTrackingContent to be displayed on top ot the item's image
 */
-(void) addContent: (CraftARTrackingContent *) content;

/**
 Remove a content from the AR item
 @param content a content that has been previously added to the AR item.
 */
-(void) removeContent: (CraftARTrackingContent *) content;

/**
 Called by the SDK when the item's tracking starts
 */
-(void) trackingStarted;

/**
 Called by the SDK when the item's tracking is lost
 */
-(void) trackingLost;

/**
 Remove all contents for this AR item
 */
-(NSArray*) allContents;

/**
 Called by the SDK when tracking events occur, updates the item position with the last result
 */
- (void) updateTracking;

/**
 * Set your custom content class for a content of a specified type
 * @param contentClass Must be a subclass of CraftARTrackingContent or any of its subclasses.
 * Instantiate the class as follows: ["My"CraftARTrackingContentImage class].
 * @param type Key for the content type (i.e. "image_button")
 */
+ (void) setContentClass: (id) contentClass forType: (NSString *) type;

@end
