// CraftARSDK_Examples is free software. You may use it under the MIT license, which is copied
// below and available at http://opensource.org/licenses/MIT
//
// Copyright (c) 2014 Catchoom Technologies S.L.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


#import "AR_ProgrammaticallyViewController.h"
#import <CraftARSDK/CraftARSDK.h>
#import <MediaPlayer/MediaPlayer.h>
#import "Reachability.h"

@interface AR_ProgrammaticallyViewController () <CraftARSDKProtocol, CraftARCloudRecognitionProtocol> {
    // CraftAR SDK reference
    CraftARSDK *_sdk;
    
    CraftARCloudRecognition *_cloudRecognition;
    CraftARTracking *_tracking;
    
    BOOL _isTrackingVideo;
    
    bool _isTrackingEnabled;
    
    BOOL _isVideoPlaying;
    
    int _num_of_vids_tracked;
    
    int _index_video_file;
    
    int _num_of_vids_playing;
    
    int checkedWifi;
//    MPMoviePlayerController * moviePlayer;
    

}

-(void) loadVideos;
-(void) removeVideos;
-(BOOL) loadSingleVideo:(NSString*) file_name;
-(BOOL) isTrackingVideos;
//-(int) checkIfAnyVideosArePlaying;
-(int) checkIfAnyVideosAreTracked;
@property (strong, nonatomic) NSMutableArray* tracked_items, *videos, *keysToRemove;//, *video_files;
@property (strong, nonatomic) NSMutableDictionary* videos_dict,*tracked_items_dict,*video_files;
@property(strong) Reachability * localWiFiReach;

@end



@implementation AR_ProgrammaticallyViewController

@synthesize tracked_items, videos, videos_dict, tracked_items_dict, video_files, scanLayer,scanOverlay, keysToRemove, wifiUILabel, localWiFiReach;

- (id)init
{
    self = [super initWithNibName:@"AR_Programmatically" bundle:nil];
    if (self != nil)
    {
        // Further initialization if needed

    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



-(void) loadVideos{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSURL *bundleRoot = [[NSBundle mainBundle] bundleURL];
    NSArray * dirContents =
    [fm contentsOfDirectoryAtURL:bundleRoot
      includingPropertiesForKeys:@[]
                         options:NSDirectoryEnumerationSkipsHiddenFiles
                           error:nil];
    NSPredicate * fltr = [NSPredicate predicateWithFormat:@"pathExtension='mp4'"];
    NSArray * onlyMP4 = [dirContents filteredArrayUsingPredicate:fltr];

    NSLog(@"%@", onlyMP4);
    
    videos_dict = [[NSMutableDictionary alloc] init];
    
    
    NSMutableArray* theFileNameKeys = [[NSMutableArray alloc] init];
    for (NSString* url in onlyMP4) {
        [theFileNameKeys addObject:[[url lastPathComponent] stringByDeletingPathExtension]];
    }
    
    NSLog(@"filenames %@",theFileNameKeys);
    
    
    video_files = [[NSMutableDictionary alloc] initWithObjects:onlyMP4 forKeys:theFileNameKeys];
    

    NSLog(@"%@",videos_dict);
    
    videos = [[NSMutableArray alloc] init];
    tracked_items_dict = [[NSMutableDictionary alloc] init];
    _index_video_file = 0;
    }

-(BOOL) loadSingleVideo:(NSString*) key{
//    NSURL *video_url = [video_files objectAtIndex:[tracked_items_dict count]];
    
    
    NSLog(@"key %@", key);
    
    NSURL* video_url = [video_files objectForKey:key];
    
     CraftARTrackingContentVideo* content_video = [[CraftARTrackingContentVideo alloc] initWithVideoFrom:video_url];
    [content_video setWrapMode:CRAFTAR_TRACKING_WRAP_ASPECT_FILL];
    
    
    [videos_dict setObject:content_video forKey:key];
    
    NSLog(@"video loaded: %@",[videos_dict objectForKey:key]);
    
    if ([videos_dict count] > 0){
        return  true;
    } else{
        return  false;
    }

}


-(int) checkIfAnyVideosAreTracked{
    int num_isTracked = 0;
    CraftARItemAR* tracked_ar_item = nil;
    if ( [tracked_items_dict count] > 0){
        for (NSString* key in tracked_items_dict) {
            tracked_ar_item = [tracked_items_dict objectForKey:key];
            NSLog(@"%@ tracked item = %i",key, [tracked_ar_item isTracked]);
            if ([tracked_ar_item isTracked]){
                num_isTracked++;
                //                break;
            }
        }
    }
    
    NSLog(@"num_isTracked %i", num_isTracked);
    return num_isTracked;
}

-(void) removeVideos{
    if ([keysToRemove count] > 0 ){
        [_tracking stopTracking];
        
        for (NSString* key in keysToRemove) {
            [_tracking removeARItem:[tracked_items_dict objectForKey:key]];
            [tracked_items_dict removeObjectForKey:key];
            [videos_dict removeObjectForKey:key];
            _isTrackingEnabled = NO;
        }
        
        [keysToRemove removeAllObjects];
        
        if ([tracked_items_dict count] > 0){
            [_tracking startTracking];
            _isTrackingEnabled = YES;
        }
    }
}

-(BOOL) isTrackingVideos{
    _num_of_vids_playing = 0;
    
    NSLog(@"videos_dict count = %lu",(unsigned long)[videos_dict count]);
    NSLog(@"tracked items count %lu",(unsigned long)[tracked_items_dict count]);
    if( [tracked_items_dict count] > 1){
        CraftARTrackingContentVideo* video;
        CraftARItemAR* item;
        
        for (NSString *key in [tracked_items_dict allKeys]) {
            video = [videos_dict objectForKey:key];
            item = [tracked_items_dict objectForKey:key];
            
            NSLog(@"%@ isTracked %i",key,[item isTracked]);
            NSLog(@"%@ isTracking %i", key,[video isTracking]);
            //                if ([item isTracked]){
            
//            NSLog(@"%li video playing %@",[video playbackStatus], key);
//            if (![item isTracked]){
            if ([video playbackStatus] != PLAYBACK_STATUS_PLAYING){
//                NSLog(@"Not playing %@ %li ",key, [video playbackStatus]);
                
                [keysToRemove addObject:key];
                
                [_cloudRecognition startFinderMode];
                //
                NSLog(@"Removed previously tracked items : %@", key);
             } else{
                _num_of_vids_playing++;
                _isTrackingVideo = YES;
//                break;
             }
        }
    }
    
    
    
    
    [_cloudRecognition startFinderMode];

    [self removeVideos];

    
    return _isTrackingVideo;
}


-(void) checkWifi{

    UIView *view = [[UIView alloc] initWithFrame:self.view.frame];
    CGSize size = view.frame.size;
    
    self.wifiUILabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.75f];
    self.wifiUILabel.layer.cornerRadius = 10.0f;
    self.wifiUILabel.layer.masksToBounds = YES;
    
    self.wifiUILabel.frame = CGRectMake(0, 0, size.width*0.8, size.height*0.1);
    
    [self.wifiUILabel setCenter:CGPointMake(size.width/2, size.height/2)];
    
    self.wifiUILabel.text = @"Your device needs to be connected to the internet";
    self.wifiUILabel.hidden = true;
    
    
    
    Reachability * wwanReach = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [wwanReach currentReachabilityStatus];
    if (networkStatus == ReachableViaWWAN) {
        // do something
        self.wifiUILabel.hidden = true;
    } else{
        self.wifiUILabel.hidden = false;
    }
    
    Reachability* wifiReach = [Reachability reachabilityForLocalWiFi];
    networkStatus = [wifiReach currentReachabilityStatus];
    if (networkStatus == ReachableViaWiFi) {
        // do something
        self.wifiUILabel.hidden = true;
        
        
        self.scanOverlay.backgroundColor = [UIColor redColor];
    } else{
        CABasicAnimation *theAnimation;
        
        theAnimation=[CABasicAnimation animationWithKeyPath:@"opacity"];
        theAnimation.duration=1.5;
        theAnimation.repeatCount=HUGE_VALF;
        theAnimation.autoreverses=YES;
        theAnimation.fromValue=[NSNumber numberWithFloat:1.0];
        theAnimation.toValue=[NSNumber numberWithFloat:0.0];
        [self.wifiUILabel.layer addAnimation:theAnimation forKey:@"animateOpacity"];
        self.wifiUILabel.hidden = false;
        self.scanOverlay.backgroundColor = [UIColor grayColor];

    }
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    
    
    __weak __block typeof(self) weakself = self;
    
    //////////////////////////////////////////////////////////////////////
    //
    // Create a reachability for the local WiFi
    
    self.localWiFiReach = [Reachability reachabilityForLocalWiFi];
    
    // we ONLY want to be reachable on WIFI - cellular is NOT an acceptable connectivity
    self.localWiFiReach.reachableOnWWAN = NO;
    
    
    self.localWiFiReach.reachableBlock = ^(Reachability * reachability)
    {
        NSString * temp = [NSString stringWithFormat:@"LocalWIFI Block Says Reachable(%@)", reachability.currentReachabilityString];
        NSLog(@"%@", temp);
//        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){

            dispatch_async(dispatch_get_main_queue(), ^{
                weakself.wifiUILabel.hidden = true;
                weakself.scanOverlay.backgroundColor = [UIColor redColor];
            });
//        });
    };

    self.localWiFiReach.unreachableBlock = ^(Reachability * reachability)
    {
        
        
        NSString * temp = [NSString stringWithFormat:@"LocalWIFI Block Says Unreachable(%@)", reachability.currentReachabilityString];
        
        NSLog(@"%@", temp);
//        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){

            dispatch_async(dispatch_get_main_queue(), ^{
                CABasicAnimation *theAnimation;
                
                theAnimation=[CABasicAnimation animationWithKeyPath:@"opacity"];
                theAnimation.duration=1.5;
                theAnimation.repeatCount=HUGE_VALF;
                theAnimation.autoreverses=YES;
                theAnimation.fromValue=[NSNumber numberWithFloat:1.0];
                theAnimation.toValue=[NSNumber numberWithFloat:0.0];
                [weakself.wifiUILabel.layer addAnimation:theAnimation forKey:@"animateOpacity"];
                
                weakself.wifiUILabel.hidden = false;
                weakself.scanOverlay.backgroundColor = [UIColor grayColor];
            });
//        });
    };
    
    [self.localWiFiReach startNotifier];
}


-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad{
    [super viewDidLoad];
//    UIView *view = [[UIView alloc] initWithFrame:self.view.frame];

//    CGSize size = view.frame.size;

//    UIView *statusBarView =  [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, 22)];
   
    //rgb(65,187,128)
//    statusBarView.backgroundColor  =  [UIColor colorWithRed:65.0 green:187.0 blue:0.0 alpha:1.0];
//    [self.view addSubview:statusBarView];

    
    _isTrackingEnabled = false;
    _isVideoPlaying = false;
    _num_of_vids_tracked = 0;
    _num_of_vids_playing = 0;
    checkedWifi = 0;
    
    
    [self loadVideos];
    tracked_items_dict = [[NSMutableDictionary alloc] init];
    keysToRemove = [[NSMutableArray alloc] init];

    
    // setup the CraftAR SDK
    _sdk = [CraftARSDK sharedCraftARSDK];
    
    // Implement the CraftARSDKProtocol to know when the previewView is ready
    [_sdk setDelegate:self];
    
//    [self reachabilityChanged:(NSNotification *)]

}




- (void) viewWillAppear:(BOOL) animated {
    [super viewWillAppear:animated];
    
    
    // Start Video Preview for search and tracking
    [_sdk startCaptureWithView: self.videoPreviewView];

}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // Stop the SDK when the view is being closed to cleanup sdk internals.
    [_sdk stopCapture];
    if (_isTrackingEnabled) {
        [_tracking stopTracking];
        [_tracking removeAllARItems];
    } else {
        [_cloudRecognition stopFinderMode];
    }
}

- (void) didStartCapture {
    // Get the CloudRecognition and set self as delegate to receive search responses
    _cloudRecognition = [_sdk getCloudRecognitionInterface];
    [_cloudRecognition setDelegate:self];
    
    // Token: d10121c873e14f32
    [_cloudRecognition setToken:@"650cc7f334e74f6c"];

//    [_cloudRecognition setToken:@"craftarexamples1"];

    // Get the Tracking instance
    _tracking = [_sdk getTrackingInterface];
    
    // Start scanning
//    self.scanOverlay.hidden = false;
    self.scanLayer.hidden = false;
    
    [self checkWifi];


    [_cloudRecognition startFinderMode];
}


//-(void) playFullScreen{
    //------------------------------------------------------
    // Play videos in full screen
    
    
    //                moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:video_url];
    //                [self.view addSubview:moviePlayer.view];
    //                moviePlayer.fullscreen = YES;
    //                [moviePlayer play];
    //            }
    
    //            } else if ([item.itemName  isEqual: @"Cube"]){
    //                NSLog(@"Recognised image %@", item.itemName);
    //
    //                video_url = [[NSBundle mainBundle] URLForResource: @"sillybubbleball" withExtension:@"mp4"];
    //                moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:video_url];
    //                [self.view addSubview:moviePlayer.view];
    //                moviePlayer.fullscreen = YES;
    //                [moviePlayer play];
    //
    //            }
    //----------------------------------------------------------
//}



- (void) didGetSearchResults:(NSArray *)results {
    Boolean haveContent = false;
//    int num_of_vids_playing = 0;
    _isTrackingVideo = NO;
    [_cloudRecognition stopFinderMode];
    
    
    // Look for trackable results
    for (CraftARItem* item in results) {
        
        if (item.getType == ITEM_TYPE_AR) {
            CraftARItemAR* arItem = (CraftARItemAR*)item;
            
            // Local content creation
            // Linda: Original code overlay with an image
            // ----------------------------------------------------------------------------
//            CraftARTrackingContentImage *image = [[CraftARTrackingContentImage alloc] initWithImageNamed:@"AR_programmatically_content" ofType:@"png"];
//            image.wrapMode = CRAFTAR_TRACKING_WRAP_ASPECT_FIT;
//            [arItem addContent:image];
//            [_tracking addARItem:arItem];
//            *video_url = [[NSBundle mainBundle] URLForResource: @"video_test" withExtension:@"mp4"];
            
            
            
            
        BOOL is_already_tracking_video = false;
//        if ([tracked_items_dict count] > 0){
        for (NSString* video_key in [tracked_items_dict allKeys]){
//        for (NSString *video_key in tracked_items_dict) {
            is_already_tracking_video = false;
            if ([item.itemName isEqualToString:video_key ]){
                NSLog(@"tracked_items count %lu", (unsigned long)[tracked_items_dict count]);
                
//                if ([tracked_items_dict count] > 0){
                NSLog(@"tracked key %@",item.itemName);
                
                if([tracked_items_dict objectForKey:item.itemName] != nil){
                    is_already_tracking_video = true;
                }
//                }
            }
            
         }
            
            if (!is_already_tracking_video){
                //                [arItem addContent:[videos_dict objectForKey:video_key]];
                if ([item.itemName isEqualToString:@"Arabella"]){
                    [tracked_items_dict setObject:arItem forKey:item.itemName];

                    arItem = [tracked_items_dict objectForKey:item.itemName];
                    [_tracking addARItem:arItem];
                    NSLog(@"arabella arItem %@", arItem);
                    haveContent = true;
                }
                
                else if ([self loadSingleVideo:item.itemName]){
//                else{
                    self.scanLayer.hidden = false;
                    [arItem addContent:[videos_dict objectForKey:item.itemName]];
                    
                    NSLog(@"arItem %@",arItem);
                    [tracked_items_dict setObject:arItem forKey:item.itemName];
                    [_tracking addARItem:[tracked_items_dict objectForKey:item.itemName]];
                    
                    //                        [_tracking addARItem:arItem];
                    _num_of_vids_tracked++;
                    if (!haveContent){
                        haveContent = true;
                    }
                }
            }

        
        NSLog(@"Recognised image %@", item.itemName);
        NSLog(@"Recognised image %@", item.imageId);
    }
}
            
            

            
            
            
//Trying to add video instead of image
//---------------------------------------------
            
//            moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:video_url];
//            [self.view addSubview:moviePlayer.view];
//            moviePlayer.fullscreen = YES;
//            [moviePlayer play];
            
//        }
//    }
    
    if (haveContent) {
        if(!_isTrackingEnabled){
            [_tracking startTracking];
            _isTrackingEnabled = true;
//            self.scanOverlay.hidden = true;
//            self.scanLayer.hidden = true;
            NSLog(@"%@ tracking",_tracking);
        }
        


    } else {
        //[_tracking removeAllARItems];//In original code
        [_cloudRecognition startFinderMode];
    }
    
    
    
//    NSLog(@"num_of_vids_playing %i",num_of_vids_playing);
    
    if([self isTrackingVideos]){
        [_cloudRecognition stopFinderMode];
        self.scanLayer.hidden = true;
       
    } else{
        self.scanLayer.hidden = false;
    }
    
    
    [_cloudRecognition startFinderMode];
    
    

    
//    NSLog(@"end of didGetResults");
}

-(void)reachabilityChanged:(NSNotification*)note
{
    Reachability * reach = [note object];
    
    if (reach == self.localWiFiReach)
    {
        if([reach isReachable])
        {
            NSString * temp = [NSString stringWithFormat:@"LocalWIFI Notification Says Reachable(%@)", reach.currentReachabilityString];
            NSLog(@"%@", temp);
            self.wifiUILabel.hidden = true;
//            self.scanOverlay.backgroundColor = [UIColor redColor];

//            self.wifiUILabel.text = temp;
//            self.wifiUILabel.textColor = [UIColor blackColor];
        }
        else
        {

            NSString * temp = [NSString stringWithFormat:@"LocalWIFI Notification Says Unreachable(%@)", reach.currentReachabilityString];
            NSLog(@"%@", temp);
            self.wifiUILabel.hidden = false;
//            self.scanOverlay.backgroundColor = [UIColor grayColor];
        }
    }
    
}


- (void) didFailWithError:(CraftARSDKError *)error {
//    NSLog(@"Error: %@", [error localizedDescription]);
    
  
}

- (void) didValidateToken {
    // Called after setToken or startSearch if the token is valid.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
